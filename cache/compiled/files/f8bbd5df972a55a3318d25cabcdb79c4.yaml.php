<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'user/plugins/error/error.yaml',
    'modified' => 1425910272,
    'data' => [
        'enabled' => true,
        'routes' => [
            404 => '/error'
        ]
    ]
];
