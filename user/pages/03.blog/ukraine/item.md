---
title: Ukraine Has Not Yet Perished
slug: ukraine-has-not-yet-perished
date: 10 Dec 2013 00:38
---

![Riot police stand ready in Ukraine](http://bc03.rp-online.de/polopoly_fs/ukrainian-riot-police-block-pro-european-1.3875258.1386606576!httpImage/3646790027.jpg_gen/derivatives/d950x950/3646790027.jpg)

Tonight, riot police line the streets of Kiev. Protestors are bedding down in tents on the Ukrainian capital's Maidan Nezalezhnosti, the big square, and there's no sign of either side backing down. For the most part, the protests and the police reaction have been peaceful, but tensions are high, as are the stakes. Let us not forget that [Ukraine's last revolution][orange-revolution] was less than ten years ago. These people believe in the power of revolution because they have lived it.

===

And we are to blame.

The EU has dangled the carrot of membership in front of Ukraine's nose for too long. It's hardly surprising that Ukraine's politicians have finally snapped and turned their back on us. In the media at the moment there's a lot of anti-Russian rhetoric, claiming the Russian administration want to exert control over Ukraine, but it's not just Putin who's been playing games.

The EU have asked for the world, and offered little in return. To fund the cuts needed to join the EU, Ukraine would need to turn to the IMF, whose position is that Ukraine would have to double utility prices while freezing salaries. The EU are not prepared to help Ukraine negotiate better terms, although they have offered to cover approximately one eighth of the country's budget deficit for this year. Due to the tax-free zone that exists between Russia and Ukraine, Russia would be forced to react punitively towards Ukraine to avoid cheap goods flooding the Russian market from the EU, destabilising the economy. Putin says he wants to join talks to discuss this issue, as well as Ukraine's pipelines, which carry a quarter of the EU's gas supplies from Russia.

So the biggest slap in the face must be [this most recent document][association-agreement], the "last straw", which the EU might as well have written in piss on a Ukrainian flag. It guarantees absolutely none of the benefits of a *rapprochement* with the EU, and by the glaring omission of any reference to membership makes it crystal clear that Ukraine will not be a member for a very long time.

The Ukrainian leadership has been placed in an impossible position. On the one hand, we have the EU, who seem hell-bent on wringing Ukraine dry, while on the other we have the protestors, desperate to escape the country's serious economic problems. We have deceived the Ukrainian people by leading them on, suggesting that we would pave the way for them to join the EU. We have not.

When the protests turn violent, we will point at Yanukovich and say that he's Putin's lapdog, and that it's his fault because he won't sign the Association Agreement. We will say that the reason he balked was because he didn't want to release [Tymoshenko][tymoshenko]. That Moscow had their hand up his backside and were moving his mouth.

But the fact is, only those who are truly guilty need a scapegoat.
<a name="timeline"><center><iframe src='http://embed.verite.co/timeline/?source=0AvubX_QWwxPjdGlkMktlbXMyQU1NdVRyazZBTEtvTkE&font=Arvo-PTSans&maptype=HYBRID&lang=en&hash_bookmark=true&start_at_slide=15&start_zoom_adjust=10&height=650' width='80%' height='650' frameborder='0' style="display:block;"></iframe></center></a>

*This article also published on [Fortitude Magazine](http://www.fortitudemagazine.co.uk/industry/politics/misguided-mission-ukraines-euromaidan/14179/)*

{<2>}![Maidan Square](http://hamodia.com/hamod-uploads/2013/12/FP-MAIN-PROTESTERS-IN-UKRAINE-KNOCK-DOWN-LENIN-MONUMENT-1.jpg)

[association-agreement]: http://euroua.com/association/eu-ukraine-association-agreement_EN.pdf
[orange-revolution]: http://en.wikipedia.org/wiki/Orange_Revolution
[tymoshenko]: http://en.wikipedia.org/wiki/Yulia_Tymoshenko
