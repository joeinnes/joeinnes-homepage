---
title: My CV
---
<h5 style="text-align: center"><a href="#about">About Me</a> | <a href="#work">Work Experience</a> | <a href="#education">Education</a> | <a href="#skills">Skills</a> | <a href="#hobbies">Hobbies</a></h5>

<a id="about"></a>
##### About Me

**Date of Birth**: 19 Jan 1988  
**Nationality**: British  
**Address**: Nagy Ignác utca 16, fsz. 2, 1055 Budapest, Hungary  
**E-mail**: [joe@joeinnes.com][email]

<a id="work"></a>
##### Work Experience
**Subject Matter Expert, Tata Consultancy Services, January '15 - present**  
After less than a year in my previous post, I was nominated as the single point of contact for our client's Business Relationship Managers based in North America, and simultaneously promoted to a Subject Matter Expert with a focus on quality, training, and reporting. This role involved handling process related issues, as well working with the upper levels of management on the client's side to ensure consistent quality of service.

**Service Desk Analyst, Tata Consultancy Services, April '14 - January '15'**  
Provided IT support for a large multi-national company. Responsibilities included diagnosis, triage, research, and resolution of users' issues with hundreds of software packages. Ensured customer satisfaction with soft skills and speedy resolutions.  
As a VIP, I was also responsible for assisting others in the team, providing training, and was shift leader and consequently the senior member of staff in the office during weekend and night shifts.
My other roles included ensuring consistent, high quality call and ticket document, administering CARM software, and managing escalations.

**Editor, [russiaSLAM], Sept ’12 - January '14**  
Translating Russian into English, launching the site, establishing a user base of approximately 25,000 per month and attracting new visitors. Featured on the Washington Post and TIME magazine websites, and in Belgian national press.

**Teacher of English as a Foreign Language and Russian, inlingua, September ’13 - February '14**  
Working with small groups and individuals both in business and outside, focusing on business English as well as general conversational English. Part of an international team with specific methodology and resources.

**TEFL Teacher, MasterClass/BIS, August 2012 – August 2013**  
Teaching English, including language specific to human resources and marketing to Russian and international clients. Class with individual students and groups from beginner to advanced in Russian companies as well as large multinationals.

**Freelance Translator, January 2013 – Present**  
Working with clients in a number of spheres to accurately and effectively convey ideas, thoughts, and messages in English originally conceived in Russian or French.

**Teacher of French, Netherhall Learning Campus, July ’11 – June ’12**  
Maternity cover replacing Head of Department. Solely responsible for languages at the school. Taught groups aged from 11-15, across the ability spectrum, working with the AQA GCSE specification. Co-tutored a form group, and supervised this group during a residential visit.

**Stagiaire, Collège Dominique Savio, June ’11**  
Taught English as a Foreign Language. Taught French students from 10-14 both about English language and culture. Prepared report on differences between TEFL and teaching of MFL in the UK. Successfully presented a case that allowed me to be the first student from the University of Sheffield to be permitted to undertake a placement abroad as part of the PGCE.

**Placement Student, Wingfield Comprehensive School, Jan – May ’11**  
Taught French to Year 8 and 10 (including top sets and more challenging classes), Russian to Year 9, prepared students for oral Controlled Assessments, worked with NVQ qualifications.

**Placement Student, Aston Comprehensive School, Oct – Dec ’10**  
Taught French to Year 7, 9, 10, 12 (including top sets and more challenging classes), prepared students for oral exams, assisted in inclusion unit, participated in team teaching.

**Placement Student, Little Sutton Primary School, Sept ’10**  
Introduced Polish and Russian to classes, assisted with literacy and numeracy teaching.

**Student Associate, Firth Park Community Arts College, June – July ’10**  
Taught French, prepared students for GCSE exams, marked Y7/8 exams in French and German.

**Business Language Champion, Apr ’10, July ’10**  
Led taster sessions in Russian language to high achieving students, assisted in the running of an enterprise day, showing students the importance of languages in business.

<a id="education"></a>
##### Education
**PGCE, The University of Sheffield, Sept ’10 – July ’11**  
Modern Foreign Languages

**BA (Hons), The University of Sheffield, Sept ’06 – July ’10**  
French & Russian (2:1, distinction in spoken French)

**A-Levels, The Arthur Terry School, Sept ’04 – July ’06**  
French (B), Maths (B), Psychology (C), AEA Psychology (Distinction)

**GCSEs, The Arthur Terry School, Sept ’99 – July ’04**  
French, German, English, Maths, Science, History, Graphic Design

<a id="skills"></a>
##### Skills
###### Language Skills
* French: CEFR C2 (Highly Proficient); studies in France.
* Russian: CEFR C1 (Proficient); capable both written and orally, good understanding; studies in Russia.
* Hungarian: CEFR A2 (Basic).
* German: CEFR Passive: B1+ (Independent); Active: A2 (Basic).
* Polish: CEFR A2 (Basic).

###### IT Skills
* Excellent understanding of hardware, software, networking, Internet, Office suites, ServiceNow, both as a user and a support agent.
* Working knowledge of SAP, AS/400, and a variety of other ERP systems.
* Understanding of HTML to a high level, able to design standards compliant websites with ease.
* Understanding of the most commonly used features of CSS.
* Understanding of JavaScript, basic PHP, and a number of other languages.

<a id="hobbies"></a>
##### Hobbies
* photography
* football
* rock climbing
* reading
* films
* cooking
* coffee
* playing guitar
* music
* socialising
* travelling

[email]: mailto:joe@joeinn.es
[russiaSLAM]: http://russiaslam.com
