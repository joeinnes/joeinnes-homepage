1426463799
s:6979:"<p><img src="http://whatwouldtotowatch.com/wp-content/uploads/2008/08/clonewars21.jpg" alt="Clone wars" /></p>
<p>War is hell. People around the globe are widely in agreement on this simple statement, which is pretty impressive, seeing as we can barely agree on the day of the week most of the time. And yet, for some reason, we’ve only managed about three years without war since WWII.</p>
<p>===</p>
<p>We’ve seen the Greek civil war, The Jewish insurgency in Palestine, the South East Asia conflicts, the Malayan emergency, the Korean war, the Anglo-Egyptian war, the Mau Mau insurgency, the Cyprus emergency, the Suez/Sinai war, the Muscat and Oman intervention, the Jordan intervention, the Indonesia conflicts, the Ugandan army mutiny, the Aden conflict, the Northern Ireland conflict, the Falklands war, both Gulf wars, the war in Afghanistan, the war in Iraq, the Former Yugoslavia peacekeeping operations and the Libyan war, and those are just the wars that <a href="http://www.historyguy.com/british_wars_1945present.htm#.UWRtYI4aySM">Britain has been involved in</a>. And it looks like things are about to get pretty intense in North Korea soon, but that’s another kettle of fish entirely.</p>
<p>These wars have always been about the deaths of individuals. The number of young men your country is able or willing to put forward to defend an ideal, attack an ideal, protect your territory, or invade someone else’s has ultimately always been the deciding factor in who gets to claim to be the winner. The science of warfare has grown around two simple premises. You can improve your chances by making it less likely that the bad guys can kill your guys, or you can make it more likely that your guys can kill the bad guys.</p>
<p>Today, I came across a video of a laser cannon mounted on a US ship destroying a US drone. It wasn’t as spectacular as I was hoping – I wanted an explosion, the drone to evaporate into thin air. That said, it was still pretty impressive. The drone burst into flame, and lost control, crashing into the sea. The video’s below, and regardless of what you think about this article, it’s worth watching.</p>
<style>
.auto-resizable-iframe {
  max-width: 420px;
  margin: 0px auto;
}

.auto-resizable-iframe > div {
  position: relative;
  padding-bottom: 75%;
  height: 0px;
}

.auto-resizable-iframe iframe {
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
}
</style>
<div class="auto-resizable-iframe">
  <div>
    <iframe frameborder="0" allowfullscreen="" src="//www.youtube.com/embed/OmoldX1wKYQ?feature=player_embedded"></iframe>
  </div>
</div>
<p>But you see, it got me thinking. There’s been a move to using UAVs in a number of theatres recently to minimise casualties among soldiers, to allow dangerous areas to be reconnoitred before troops move in, or, in some cases, to actually launch strikes against the enemy without our troops ever moving within rifle range. It is, no doubt, an incredible feat of engineering. The drones themselves are sophisticated, and the amount of work that has gone into trying to ensure our troops’ safety is phenomenal.</p>
<p>Think about how far we’ve come since the first battles were fought. One man against another for a hunk of meat developed into a group of men against another group of men for land and resources. At some point, it is believed that the Sumerians were the first to train and equip what we would call an army. This was significant, because it was no longer citizen against citizen, it was trained and armed professionals fighting against each other to demonstrate their superiority. In a sense, this created the first “civilians” in the sense that we use the word today. There were now people who didn’t fight, because others did the fighting for them. Society has benefited immensely from this. People could get on with making the world around them a better place, and could specialise in skills like science, mathematics, agriculture. And it has led us to the point we find ourselves at today.</p>
<p>Warfare has always been relatively symmetrical. Introducing this level of machine-versus-machine technology into warfare makes for some interesting philosophical questions (if a drone falls in the woods, and no-one notices it’s gone, does it matter if it was shot down or if it was ‘operator error’?), but most significantly, it forces the opponent to make a choice. They have three options:</p>
<ol>
<li>Continue to send young men to their deaths, despite knowing that they will never get close enough to a real live enemy to have an effect on the outcome of the war.</li>
<li>Adapt, and develop machine weapons of their own, which abstracts the conflict into a question of “who has more resources”, that could in fact be answered without any material losses for either side.</li>
<li>Adopt an asymmetrical strategy. Begin a campaign of terror. Start to target the public. The number of civilian casualties would drastically increase, but the number of soldiers who die in combat would plummet.</li>
</ol>
<p>In the past, war has been an effective (if rather uncivilised) way of resolving disputes because the stakes are so high. We roll the dice against each other, and the losses are painful. By introducing technology like this, we are effectively changing the stakes, and what’s worse, it’s only those who can afford the big losses who have access to the technology and resources to allow them to avoid them.</p>
<p>This doesn’t mean that wars will cease to occur. It means that the nature of war will change. The Goliathan countries that can afford the technology will soon be faced with Davids who can’t compete in terms of might, and so will attack where the armour is weak. There are a lot of parallels I can draw with the story of David and Goliath, but the most important is to remember that David wins … and he was the good guy (well … despite going on to have more than ten wives and another ten concubines, sleeping with Bathsheba, the wife of one of his top men, then trying to get him killed so he could add her to his hareem).</p>
<p>Terrorism will become the only way for a vastly outnumbered and outgunned opponent to resolve issues outside diplomatic channels. We will blame it on the opponents being cruel and inhuman, as we have done for thousands of years, but the truth is that we will have brought it on ourselves, by taking away the possibility for armed combat to inflict serious losses.</p>
<p>“It’s the army’s job to die for their country”, is often countered with the rejoinder that “it’s the army’s job to <em>fight</em> for their country”. But this simply doesn’t make sense. As long as people are prepared to kill for what they believe in, we need people who are prepared to die for what they believe in too. Because if we swap our battlefields and armies for drones and cruise missiles, then the people that die will be ordinary civilians.</p>";