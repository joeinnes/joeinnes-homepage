---
title: ASBO 2.0 Rejected - But the Problem Remains
slug: asbo-2-0
date: 04 Feb 2014 19:46
---

![Footballers](http://www.fortitudemagazine.co.uk/wp-content/uploads/2014/01/9882373434_f1ee24a0f2_o-11-620x350.jpg)

When even the [Torygraph](http://www.telegraph.co.uk/news/uknews/law-and-order/10472978/What-does-Ipna-stand-for-Not-law-and-order.html) can't get behind a new law and order initiative, you can be sure it's badly flawed. Such was the case with the Ipnas, a new piece of legislation intended to replace the infamous Asbo.

===

The Ipnas were conceived to broaden the powers the police have to prevent annoying behaviour. That's not a joke, by the way, it's there in the name. The full name of the Ipnas was "Injunction to Prevent Nuisance and Annoyance". Fortunately, the proposal was rejected by the House of Lords for being too far-reaching and not specific enough. After all, "nuisance" and "annoyance" are both very subjective terms, and are pretty low barriers. While breaching an Ipna was not in itself a criminal offence, it carried a maximum prison sentence of two years.

So, the current system remains, which everyone should be pleased about, right? Wrong.

Asbos, which have made the press repeatedly in their sixteen years of existence, are hardly a fair alternative. There are numerous reasons Asbos represent a massive infringement on human rights.

According to [the law](http://www.legislation.gov.uk/ukpga/1998/37/section/1), an Asbo can be given for:

> conduct which caused or was likely to cause harm, harassment, alarm or distress, to one or more persons not of the same household as him or herself and where an ASBO is seen as necessary to protect relevant persons from further anti-social acts by the defendant._

On first reading, this seems to be fair. But when you look at the laundry list of all the different things you can be given an Asbo for, it starts to look a little less reasonable. For example, Asbos have been given out for "loitering", a useful term that allows anyone who spends any time in a specific place to be accused of some nefarious deed. One boy is forbidden from playing football in his street. An 87-year-old has been told he is no longer allowed to be sarcastic to his neighbours. Children as young as three [have been threatened](http://metro.co.uk/2013/08/15/children-get-police-warning-letters-for-intimidating-and-antisocial-behaviour-because-they-played-outside-3925725/) with Asbos for playing in the park near their houses. Asbos can be given for practically any reason, and may require almost anything of the defendant (although to clarify, the defendant can only be told **not** to do something).

On the other end of the scale, you can be given an Asbo for paedophilia.

So why would Asbos be used for things like paedophilia when there are perfectly solid laws already on the books to combat them? The answer lies in the manner in which they are given. They are issued by magistrates' courts, and while they are supposed to try to the same standards of proof as a Crown court (ie: beyond reasonable doubt), magistrates are accustomed to hearing civil cases where the balance of probabilities is enough to find in favour of an applicant. It's also possible to do away with that pesky jury that returns so many "not guilty" verdicts. In fact, only 3% of Asbo applications are rejected.

But the real problem with Asbos is that breaking an Asbo is a criminal offence, independently of what the Asbo was initially given for. For example, if the fifteen year old boy mentioned above decides to go for a kick about in his street, he has clearly and without question broken the law and can be sentenced to prison for breaching the terms of his Asbo.

It's easy to see how the police can use this to create new laws without any judicial oversight. All they need to do is issue an Asbo, and wait for it to be broken. Which happens about 70% of the time nationally, although the figure reaches 90% in some areas.

Asbos are a shortcut to turning otherwise law-abiding citizens into criminals, and a shortcut that needs serious re-examination. Let us not allow the recent rejection of the Ipnas distract us from the injustice and lack of due process Asbos represent.
