<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'user/config/site.yaml',
    'modified' => 1425910272,
    'data' => [
        'title' => 'Joe Innes',
        'author' => [
            'name' => 'Joe Innes',
            'email' => 'joe@joeinn.es'
        ],
        'metadata' => [
            'description' => 'Joe Innes\'s personal website'
        ],
        'blog' => [
            'route' => '/blog'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ]
    ]
];
