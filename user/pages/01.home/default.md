---
title: Home
---

<img src="moscow.png" width="100%" />

#### I'm an IT professional with a proven track record of problem-solving and delivering quality service

Follow me on social media:  
[Facebook][fb]  
[Twitter][twitter]  
[LinkedIn][linkedin]  
[GitHub][github]  
[Last.fm][lastfm]  

[fb]: http://facebook.com/joeinnes
[twitter]: http://twitter.com/d1sxeyes
[linkedin]: https://www.linkedin.com/profile/view?id=225503916
[github]: https://github.com/joeinnes
[lastfm]: http://www.last.fm/user/Mangula_D1S
