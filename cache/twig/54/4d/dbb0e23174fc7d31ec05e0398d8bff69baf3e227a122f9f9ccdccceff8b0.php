<?php

/* item.html.twig */
class __TwigTemplate_544ddbb0e23174fc7d31ec05e0398d8bff69baf3e227a122f9f9ccdccceff8b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("item.html.twig", "131486880")->display($context);
    }

    public function getTemplateName()
    {
        return "item.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}


/* item.html.twig */
class __TwigTemplate_544ddbb0e23174fc7d31ec05e0398d8bff69baf3e227a122f9f9ccdccceff8b0_131486880 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        try {
            $this->parent = $this->env->loadTemplate("partials/base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["base_url"] = $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "parent", array()), "url", array());
        // line 4
        $context["feed_url"] = (isset($context["base_url"]) ? $context["base_url"] : null);
        // line 6
        if (((isset($context["base_url"]) ? $context["base_url"] : null) == "/")) {
            // line 7
            $context["base_url"] = "";
        }
        // line 10
        if (((isset($context["base_url"]) ? $context["base_url"] : null) == (isset($context["base_url_relative"]) ? $context["base_url_relative"] : null))) {
            // line 11
            $context["feed_url"] = (((isset($context["base_url"]) ? $context["base_url"] : null) . "/") . $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "parent", array()), "slug", array()));
        }
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "\t\t";
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "plugins", array()), "breadcrumbs", array()), "enabled", array())) {
            // line 16
            echo "\t\t\t";
            $this->env->loadTemplate("partials/breadcrumbs.html.twig")->display($context);
            // line 17
            echo "\t\t";
        }
        // line 18
        echo "
\t\t<div class=\"blog-content-item grid pure-g-r\">
\t\t\t<div id=\"item\" class=\"block pure-u-3-3\">
\t\t\t    ";
        // line 21
        $this->env->loadTemplate("partials/blog_item.html.twig")->display(array_merge($context, array("truncate" => false, "big_header" => true)));
        // line 22
        echo "\t\t\t</div>
\t\t</div>
\t";
    }

    public function getTemplateName()
    {
        return "item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 22,  98 => 21,  93 => 18,  90 => 17,  87 => 16,  84 => 15,  81 => 14,  77 => 1,  74 => 11,  72 => 10,  69 => 7,  67 => 6,  65 => 4,  63 => 3,  19 => 1,);
    }
}
